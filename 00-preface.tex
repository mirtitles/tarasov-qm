% !TEX root = tarasov-qm.tex

\chapter*{Preface}
\phantomsection
\addcontentsline{toc}{chapter}{Preface}
%\markboth{\textsc{Preface}}{\textsc{Preface}}
Research in physics, \marginnote{Some Preliminary Remarks}conducted at the end of the $19^{\text{th}}$ century and in the first half of the $20^{\text{th}}$ century, revealed exceptionally peculiar nature of the laws governing the behaviour of microparticles-atoms, electrons, and so on. On the basis of this research a new physical theory called \emph{quantum mechanics} was founded. 

The growth of quantum mechanics turned out to be quite complicated and prolonged. The mathematical part of the theory, and the rules linking the theory with experiment, were constructed relatively quickly (by the beginning of the thirties). However, the understanding of the physical and philosophical substance of the mathematical symbols used in the theory was unresolved for decades. In Fock's words, 
\begin{quote}%
The mathematical apparatus of non-relativistic quantum mechanics worked well and was free of contradictions; but in spite of many successful applications to different problems of atomic physics the physical representation of the mathematical scheme still remained a problem to be solved.\cite{fock-1957}
\end{quote}

Many difficulties are involved in a mathematical interpretation of the quantum-mechanical apparatus. These are associated with the dialectics of the new laws, the radical revision of the very nature of the questions which a physicist ``is entitled to put to nature'', the reinterpretation of the role of the observer \emph{vis-\'a-vis} his surroundings, the new approach to the question of the relation between chance and necessity in physical phenomena, and the rejection of many accepted notions and concepts. Quantum mechanics was born in an atmosphere of discussions and heated clashes between contradictory arguments. The names of many leading scientists are linked with its development, including N.~Bohr, A.~Einstein, M.~Planck, E.~Schr\"odinger, M.~Born, W.~Pauli, A.~Sommerfeld, L.~de Broglie, P.~Ehrenfest, E.~Fermi, W.~Heisenberg, P.~Dirac, R.~Feynman, and others.

It is also not surprising that even today anyone who starts studying quantum mechanics encounters some sort of psychological barrier. This is not because of the mathematical complexity. The difficulty arises from the fact that it is difficult to break away from accepted concepts and to reorganize one's pattern of thinking which are based on everyday experience.

Before starting a study of quantum mechanics, it is worthwhile getting an idea about its place and role in physics. We shall consider (naturally in the most general terms) the following three questions: 
%\begin{tcolorbox}[,breakable,colframe=darkgray,colback=lightgray,boxrule=0pt]%[style=grayroundbox]
%\smaller
%{\sf
\begin{fullwidth}
\begin{enumerate}[label=\textsection,noitemsep]
\item What is quantum mechanics? 
\item What is the relation between classical physics and quantum mechanics? 
\item What specialists need quantum mechanics? 
\end{enumerate}
\end{fullwidth}
%}
%\end{tcolorbox}
So, what is quantum mechanics? 

The question can be answered in different ways. First and foremost, quantum mechanics is a theory describing the properties of matter at the level of \emph{microphenomena} -- it considers the laws of motion of \emph{microparticles}\index{Microparticle}. Microparticles (molecules, atoms, elementary particles) are the main ``characters'' in the drama of quantum mechanics.

From a broader point of view quantum mechanics should be treated as the theoretical foundation of the modern theory of the structure and properties of matter. In comparison with classical physics, quantum mechanics considers the properties of matter \emph{on a deeper and more fundamental level}. It provides answers to many questions which remained unsolved in classical physics. For example, why is a diamond hard? Why does the electrical conductivity of a semiconductor increase with temperature? Why does a magnet lose its properties upon heating? Unable to get answers from classical physics to these questions, we turn to quantum mechanics. Finally, it must be emphasized that quantum mechanics allows one to calculate many physical parameters of substances. Answering the question ``What is quantum mechanics?'', Lamb remarked: 
\begin{quote}
The only easy one (answer) is that quantum mechanics is a discipline that provides a wonderful set of rules for calculating physical properties of matter.\cite{lamb-1969}
\end{quote}
What is the relation of quantum mechanics to classical physics? First of all quantum mechanics includes classical mechanics as a limiting (extreme) case. Upon a transition from microparticles to macroscopic bodies, quantum-mechanical laws are converted into the laws of classical mechanics. Because of this it is often stated, though not very accurately, that quantum mechanics ``works'' in the microworld and the classical mechanics, in the macroworld. This statement assumes the existence of an isolated ``microworld'' and an isolated ``macroworld''. In actual practice we can only speak of microparticles (microphenomena) and macroscopic bodies (macrophenomena). It is also significant that microphenomena form the basis of macrophenomena and that macroscopic bodies are made up of microparticles. Consequently, the transition from classical physics to quantum mechanics is a transition not from one ``world'' to another, but from a shallower to a deeper level of studying matter. This means that in studying the behaviour of microparticles, quantum mechanics considers in fact the same macroparticles, but on a more fundamental level. Besides, it must be remembered that the boundary between micro- and macro- phenomena in general is quite conditional and flexible. Classical concepts are frequently found useful when considering microphenomena, while quantum-mechanical ideas help in the understanding of macrophenomena. There is even a special term ``quantum macrophysics'' which is applied, in particular, to quantum electronics, to the phenomena of superfluidity and superconductivity and to a number of other cases.

In answering the question as to what specialists need quantum mechanics, we mention beforehand that we have in mind specialists training in engineering colleges. There are at least three branches of engineering for which a study of quantum mechanics is absolutely essential. Firstly, there is the field of nuclear power and the application of radioactive isotopes to industry. Secondly, the field of materials sciences (improvement of properties of materials, preparation of new materials with preassigned properties). Thirdly, the field of electronics and first of all the field of semiconductors and laser technology. If we consider that today almost any branch of industry uses new materials as well as electronics on a large scale, it will become clear that a comprehensive training in engineering is impossible without a serious study of quantum mechanics.
%\vspace{.5cm}

The aim of this book\marginnote{The Structure of the Book} is to acquaint the reader with the concepts and ideas of quantum mechanics and the physical properties of matter; to reveal the logic of its new ideas, to show how these ideas are embodied in the mathematical apparatus of linear operators and to demonstrate the working of this apparatus using a number of examples and problems of interest to engineering students.

The book consists of three chapters. By way of an introduction to
quantum mechanics, the \hyperref[ch-01]{first chapter} includes a study of the physics of microparticles. Special attention has been paid to the fundamental ideas of quantization\index{Quantization} and duality as well as to the uncertainty relations.\index{Uncertainty relations} The first chapter aims at ``introducing'' the main ``character'', i.e. the microparticle, and at showing the necessity of rejecting a number of concepts of classical physics.

The \hyperref[ch-02]{second chapter} deals with the physical concepts of quantum mechanics. The chapter starts with an analysis of a set of basic experiments which form a foundation for a system of quantum-mechanical ideas. This system is based on the concept of the amplitude of transition probability.\index{Amplitude!transition probability} The rules for working with amplitudes are demonstrated on the basis of a number of examples, the interference of amplitudes\index{Interference of probability amplitudes} being the most important. The principle of superposition\index{Principle!superposition of states} and the measurement process are considered. This concludes the first stage in the discussion of the physical foundation of the theory. In the second stage an analysis is given based on amplitude concepts of the problems of causality in quantum mechanics. The Hamiltonian matrix\index{Hamiltonian!matrix}\index{Energy!matrix} is introduced while considering causality and its role is illustrated using examples involving microparticles with two basic states, with emphasis on the example of an electron in a magnetic field. The chapter concludes with a section of a general physical and philosophical nature.

The \hyperref[ch-03]{third chapter} deals with the application of linear operators in the apparatus of quantum mechanics. At the beginning of the chapter the required mathematical concepts from the theory of Hermitian and unitary linear operators are introduced. It is then shown how the physical ideas can be ``knitted'' to the mathematical symbols, thus changing the apparatus of operator theory into the apparatus of quantum theory. The main features of this apparatus are further considered in a concrete form in the framework of the coordinate representation\index{Coordinate representation}\index{Representation!coordinate}. The transition from the coordinate to the momentum representation is illustrated.\index{Representation!transition from coordinate to momentum} Three ways of describing the evolution of microsystems in time, corresponding to the Schr\"odinger, Heisenberg and Dirac representation\index{Representation!Dirac's}\index{Representation!Schr\"odinger}\index{Representation!Heisenberg}, have been discussed. A number of typical problems are considered to demonstrate the working of the apparatus; particular attention is paid to the problems of the motion of an electron in a periodic field and to the calculation of the probability of a quantum transition.

The book contains a number of \emph{interludes}. These are dialogues in which the author has allowed himself free and easy style of considering certain questions. The author was motivated to include interludes in the book by the view that one need not take too serious an attitude when studying serious subjects. And yet the reader should take the interludes fairly seriously. They are intended not so much for mental relaxation, as for helping the reader with fairly delicate questions, which can be understood best through a flexible dialogue treatment. 

Finally, the book contains many quotations. The author is sure that the ``original words'' of the founders of quantum mechanics will offer the reader useful additional information.

%\vspace{0.5cm}

The author wishes to express his deep gratitude \marginnote{Personal Remarks}to Prof. I.I. Gurevich, Corresponding Member of the USSR Academy of Sciences, for the stimulating discussions which formed the basis of this book. Prof. Gurevich discussed the plan of the book and its preliminary drafts, and was kind enough to go through tho manuscript. His advice not only helped mould the structure of the book, but also helped in the nature of exposition of the material. The subsection ``The Essence of Quantum Mechanics'' in \hyperref[sec-16]{Section \ref{sec-16}} is a direct consequence of Prof. Gurevich's ideas. 

The author would like to record the deep impression left on him by the
works on quantum mechanics by the leading American physicist
R. Feynman.\cite{feynman-1965a,feynman-1965b} While reading the
sections in this book dealing with the applications of the idea of
probability amplitude, superposition principle, microparticles with
two basic states, the reader can easily detect a definite similarity
in approach with the corresponding parts in Feynman's ``Lectures in
Physics''. The author was also considerably influenced by N.~Bohr (in
particular by his wonderful essays \emph{Atomic Physics and Human
  Knowledge})\cite{bohr-1958b}, V. A. Fock\cite{fock-1978,fock-1957},
W. Pauli\cite{pauli-1946}, P. Dirac\cite{dirac-1958}, and also by the comprehensive works of L.~D. Landau and E. M. Lifshitz\cite{landau-1977}, D. I. Blokhintsev\cite{blokhintsev-1964}, E. Fermi\cite{fermi-1961}, L. Schiff\cite{schiff-1968}.
	
The author is especially indebted to Prof. M. I. Podgoretsky, D.Sc., for a thorough and extremely useful analysis of the manuscript. He is also grateful to Prof. Yu. A. Vdovin, Prof. E. E. Lovetsky, Prof. G. F. Drukarev, Prof. V. A. Dyakov, Prof. Yu. N. Pchelnikov, and Dr. A. M. Polyakov, all of whom took the trouble of going through the manuscript and made a number of valuable comments. Lastly, the author is indebted to his wife Aldina Tarasova for her constant interest in the writing of the book and her help in the preparation of the manuscript. But for her efforts, it would have been impossible to bring the book to its present form.
\cleardoublepage


%\begin{savequote}[150mm]
%  ``\emph{Evidence presentations are seen here from both sides: how to
%  \emph{produce} them and how to \emph{consume} them. As teachers
%  know, a good way to learn something is to teach it. The partial
%  symmetry of producers and consumers is a consequence of analytical
%  design, which is based on the premise that the \emph{point of
%    evidence displays is to assist the thinking of producer and
%    consumer alike}}.'' (emphasis in original)
%
%  \qauthor{Edward Tufte, \emph{Beautiful Evidence, 2006}}

%\end{savequote}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tarasov-qm"
%%% TeX-engine: xetex
%%% End:
