% !TEX root = tarasov-qm.tex
%\begin{savequote}
%
%\qauthor{Goethe (Faust)}
%\end{savequote}

\chapter*{Prelude: Can the System of \newline Classical Physics Concepts Be \newline Considered Logically Perfect?}
\phantomsection
\addcontentsline{toc}{chapter}{Prelude: Can the System of Classical
  Physics Concepts Be Considered Logically Perfect?}\label{interlude-01}
%\begin{texttt}
%[fontfamily=lcmtt]
%\verbatimfontfamily{lcmtt}
%[formatcom=\color{darkgray},fontsize=\footnotesize]
\begin{verbatim}
He who would study organic existence,
First drives out the soul with rigid persistence,
Then the parts in his hands he may hold and class,
But the spiritual link is lost, alas!
Goethe (Faust)
\end{verbatim}
%\end{texttt}
%\indent
%{\parindent=0pt

\begin{dialogue}

\athr It is well known \marginnote{Participants: the Author and the Classical Physicist (Physicist of the older generation, whose views have been formed on the basis of classical physics alone).}that the basic contents of a physical theory are formed by a system of concepts which reflect the objective laws of nature within the framework of the given theory. Let us take the system of concepts lying at the root of classical physics. Can this system be considered logically perfect?

\clphy It is quite perfect. The concepts of classical physics were formed on the basis of prolonged human experience; they have stood the test of time.

\athr What are the main concepts of classical physics?

\clphy I would indicate three main points: 
\begin{enumerate*}[label=(\alph*)]
\item continuous variation of physical quantities;
\item the principle of classical determinism; 
\item the analytical method of studying objects and phenomena. 
\end{enumerate*}
While talking about \emph{continuity}, let us remember that the state of an object at every instant of time is completely determined by describing its coordinates and velocities, which are continuous functions of time. This is what forms the basis of the concept of motion of objects along trajectories. The change in the state of an object may in principle be made as small as possible by reducing the time of observation. 

\emph{Classical determinism}\index{Classical determinism} assumes that if the state of an object as well as all the forces applied to it are known at some instant of time, we can precisely predict the state of the object at any subsequent instant. Thus, if we know the position and velocity of a freely falling stone at a certain instant, we can precisely tell its position and velocity at any other instant, for example, at the instant when it hits the ground.

\athr In other words, classical physics assumes an unambiguous and inflexible link between present and future, in the same way as between past and present.

\clphy The possibility of such a link is in close agreement with the continuous nature of the change of physical quantities: for every instant of time we always have an answer to two questions: ``What are the coordinates of an object?'' and, ``How fast do they change?'' Finally, let us discuss the \emph{analytical method} of studying objects and phenomena. Here we come to a very important point in the system of concepts of classical physics. The latter treats matter as made up of different parts which, although they interact with one another, may be investigated individually. This means that firstly, the object may be \emph{isolated} from its environments and treated as an independent entity, and secondly, the object may be \emph{broken up}, if necessary, into its constituents whose analysis could lead to an understanding of the nature of the object.

\athr It means that classical physics reduces the question ``what is an object like?'' to ``what is it made of?''

\clphy Yes, indeed. In order to understand any apparatus we must ``dismantle'' it, at least in one's imagination, into its constituents. By the way, everyone tries to do this in his childhood. The same is applicable to phenomena: in order to understand the idea behind some phenomenon, we have to express it as a function of time, i.e. to find out what follows what.

\athr But surely such a step will destroy the notion of the object or phenomenon as a single unit.

\clphy To some extent. However, the extent of this ``destruction'' can be evaluated each time by taking into account the interactions between different parts and relation between the time stages of a phenomenon. It may so happen that the initially isolated object (a part of it) may considerably change with time as a result of its interaction with the surroundings (or interaction between parts of the object). However, since these changes are continuous, the individuality of the isolated object can always be returned over any period of time. It is worthwhile to stress here the internal logical connections among the three fundamental notions of classical physics.

\athr I would like to add that one special consequence of the ``principle of analysis'' is the notion, characteristic of classical physics, of the mutual independence of the object of observation and the measuring instrument (or observer). We have an instrument and an object of measurement. They can and should be considered separately, independently from one another.

\clphy Not quite independently. The inclusion of an ammeter in an electric circuit naturally changes the magnitude of the current to be measured. However, this change can always be calculated if we know the resistance of the ammeter.

\athr When speaking of the independence of the instrument and the object of measurement, I just meant that their interaction may be simply ``ignored''.

\clphy In that case I fully agree with you.

\athr Born has considered this point in \emph{Physics in My
Generation}.\cite{born-1956} Characterizing the philosophy of science which influenced ``people of older generation'', he referred to the tendency to consider that the object of investigation and the investigator are completely isolated from each other, that one can study physical phenomena without interfering with their passage. Born called such style of thinking ``Newtonian'', since he felt that this was reflected in ``Newton's celestial mechanics.''

\clphy Yes, these are the notions of classical physics in general terms. They are based on everyday commonplace experience and it may be confidently stated that they are acceptable to our common sense, i.e. are taken as quite natural. I rather believe that the ``principle of analysis'' is not only a natural but the only effective method of studying matter. It is incomprehensible how one can gain a deeper insight into any object or phenomenon without studying its components. As regards the principle of classical determinism, it reflects the causality of phenomena in nature and is in full accordance with the idea of physics as an \emph{exact science}.

\athr And yet there are grounds to doubt the
``flawlessness'' of classical concepts even from very general
considerations. 

Let us try to extend the principle of classical determinism to the universe as a whole, We must conclude that the positions and velocities of all ``atoms'' in the universe at any instant are precisely determined by the positions and velocities of these ``atoms'' at the preceding instant. Thus everything that takes place in the world is \emph{predetermined beforehand}, all the events can be fatalistically predicted. According to Laplace, we could imagine some ``super-being'' completely aware of the future and the past. In his \emph{Theorie analytique des probabilites}, published in 1820, Laplace wrote\cite{laplace-1952}:
%\begin{quote}
\begin{quoting}
An intelligence knowing at a given instant of time all forces acting
in nature as well as the momentary positions of all things of which
the universe consists, would be able to comprehend the motions of the
largest bodies of the world and those of the lightest atoms in one
single formula, provided his intellect were sufficiently powerful to
subject all data to analysis, to him nothing would be uncertain, both
past and future would be present to his eyes. 
\end{quoting}
%\end{quote}
It can be seen that an imaginary attempt to extend the principle of
classical determinism to nature in its entity leads to the emergence
of the idea of fatalism, which obviously cannot be accepted by common
sense. 

Next, let us try to apply the ``principle of analysis'' to an investigation of the structure of matter. We shall, in an imaginary way, break the object into smaller and smaller fractions, thus arriving finally at the molecules constituting the object further ``breaking-up'' leads us to the conclusion that molecules are made up of atoms. We then find out that atoms are made up of a nucleus and electrons. Accustomed to the tendency of splitting, we would like to know what an electron is made of. Even if we were able to get an answer to this question, we would have obviously asked next: What are the constituents, which form an electron, made of? And so on. We tend to accept the fact that such a ``chain'' of questions is endless. The same common sense will revolt against such a chain even though it is a direct consequence of classical thinking. 

Attempts were made at different times to solve the problem of this chain. We shall give two examples here. The first one is based on Plato's views on the structure of matter. He assumed that matter is made up of four ``elements'' - earth, water, air and fire.

Each of these elements is in turn made of atoms having definite geometrical forms. The atoms of earth are cubic, those of water are icosahedral; while the atoms of air and fire are octahedral and tetrahedral, respectively. Finally, each atom was reduced to triangles. To Plato, a triangle appeared as the simplest and most perfect mathematical form, hence it cannot be made up of any constituents. In this way, Plato reduced the chain to the purely mathematical concept of a triangle and terminated it at this point.

The other example is characteristic for the beginning of the
$20^{\text{th}}$ century. It makes use of the external similarity of
form between the planetary model of the atom and the solar system. It
is assumed that our solar system is nothing but an isolated atom of
some other, gigantic world, and an ordinary atom is a sort of ``solar
system'' for some third dwarfish world for which ``our electron'' is
like a planet. In this case we admit the existence of an infinite row
of more and more dwarfish worlds, just like more and more gigantic
worlds. In such a system the structure of matter is described in
accordance with the primitive ``chinese box'' principle. The ``chinese
box'' principle of hollow tubes, according to which nature has a more
or less similar structure, was not accepted by all the physicists of
older generations. However, this principle is quite characteristic of
classical physics, it conforms to classical concepts, and follows
directly from the classical principle of analysis. In this connection,
criticizing Pascal's views that the smallest and the largest objects
have the same structure, Langevin\cite{langevin-1934} pointed out
that this would lead to the same aspects of reality being revealed at
all levels. The universe should then be reflected in an absolutely
identical fashion in all objects, though on a much smaller
scale. Fortunately, reality turns out to be much more diverse and
interesting. 

Thus, we are convinced that a successive application of the principles
of classical physics may, in some cases, lead to results which appear
doubtful. This indicates the existence of situations for which
classical principles are not applicable. Thus it is to be expected
that for a sufficiently strong ``breaking-up'' of matter, the
principle of analysis must become redundant (thus the idea of the
independence of the object of measurement from the measuring
instrument must also become obsolete). In this context the question
``what is an electron made of?'' would simply appear to have lost its
meaning. 

If this is so, we must accept the \emph{relativity} of the classical concepts which are so convenient and dear to us, and replace them with some qualitatively new ideas on the motion of matter. The classical attempts to obtain an endless detailization of objects and phenomena mean that the desire inculcated in us over centuries ``to study organic existence'' leads at a certain stage to a ``driving out of the soul'' and a situation arises, where, according to Goethe, ``the spiritual link is lost''.%}
\end{dialogue}
\cleardoublepage

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tarasov-qm"
%%% TeX-engine: xetex
%%% End:
