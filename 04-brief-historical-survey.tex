% !TEX root = tarasov-qm.tex

\chapter[A Brief Historical Survey]{On the
  History of Origin and Growth of Quantum Mechanics (A Brief
  Historical Survey}

 Nineteenth century \marginnote{``The Crisis in Physics"}was an era
  of rapid growth in physics. It is enough to mention just a few
  areas: the achievements in electricity and magnetism which led to
  Maxwells electromagnetic field theory and permitted the inclusion of
  optics into the framework of electromagnetic phenomena; the
  significant progress in the development of classical mechanics
  which came close to perfection as the result of a number of
  brilliant mathematical works; the enunciation of many universal
  principles in physics, of prime importance among them being the law
  of conservation and transformation of energy. It is not astonishing
  that towards the end of the $19^{\textrm{th}}$ century it was generally believed   that the description of the laws of nature was in a final stage. In
  this respect the famous remarks of Planck are worth noting. After
  defending his Ph.D. thesis, Planck wrote to his teacher and mentor
  Philip Jolly asking his advice as to whether he should seek a career
  in theoretical physics. 
  \begin{quoting}
  ``Young man'', \textrm{replied Jolly}, ``Why do you want to ruin your life? The theoretical physics is practically finished,
  the differential equations have all been solved. AII that is left
  now is to consider individual special cases involving variations of
  initial boundary conditions. Is it worthwhile taking up a job
  which does not hold any prospects for the future?''
  \end{quoting}

In the August of 1900, the celebrated mathematician Hilbert presented
his famous 23 problems before the Second International Congress of
Mathematicians. One of these problems (the sixth one) pertained to
axiomation in physics. Hilbert proposed that a finite number of
initial axioms should be formulated so that all the results needed for
a \emph{complete} description of the physical picture of the world could be
obtained from them by purely logical means. The very fact that such a
problem was stated speaks in most convincing manner of the belief held
by scientists at the time that a final description of physical
phenomena was at hand. 

Events which followed dispelled such illusions very soon. At the turn
of the $20^{\textrm{th}}$ century a number of fundamental discoveries which could not be contained within the framework of the existing theories in physics were reported. The list of these discoveries was quite
imposing: $X$-rays, the dependence of the mass of an electron on its
velocity, the incomprehensible laws of the photoelectric effect,
radioactivity, etc. It appeared that nature had decided to ``laugh'' at
the self-confidence of people who thought they had uncovered all its
secrets.  

Such an unexpected turn of events forced many physicists and
philosophers to speak of a \emph{collapse} of the earlier foundations, of
the impossibility of knowing everything about matter, of the absence
of objective laws of nature, of the ``disappearance of mass'', etc. The
earlier unanimity was replaced by sharp differences of opinion about
fundamental ideas. 

In his book ``Materialism and Empirio-criticism''
published in 1908, V I. Lenin called this period in the development of
physics a period of ``crisis in physics'': 
\begin{quoting}
The essence of the crisis in 
modern physics consists in the break-down of the old laws and basic
principles, in the rejection of an objective reality existing
outside the mind, that is, in the replacement of materialism by
idealism and agnosticism. `Matter has disappeared' -- one may thus
express the fundamental and characteristic difficulty in relation to
many particular questions which has created this crisis.
\end{quoting}

Analysing the causes that led to this crisis, Lenin wrote: \emph{It is
mainly because the physicists did not know dialectics that the new
physics strayed into idealism.}

Defending the dialectical point of view, Lenin emphasized that
\begin{quoting}
`Matter disappears' means that the limit within which we have hitherto known matter disappears and that our knowledge is penetrating
deeper.
\end{quoting}

Lenin pointed out that the period of crisis will culminate in a new
leap in the development of physics and that its further development
will occur on the lines of materialistic dialectics. He wrote:
\begin{quoting}
Modern physics is in travail; it is giving birth to dialectical
materialism. The process of child-birth. is painful.
\end{quoting}

Looking back, we can now say that this ``travail'' led also to the birth
of quantum mechanics. As Lenin envisaged, the overcoming of this
``crisis in physics'' resulted in a deepening of our knowledge of
matter, and demanded a decisive turn from metaphysical to dialectical
ideas. This was reflected most clearly in the quantum-mechanical
concepts. We have every reason to associate quantum mechanics with a
\emph{qualitative leap} in our knowledge about nature (see \sect{sec-16}).

Considering the history of the initial period of quantum mechanics, we
can isolate three distinct stages. 
\begin{enumerate}[leftmargin=1cm,label=\textsection]
\item The \emph{first stage}: end of 19th
century-1912 (first experiments and first attempts to explain
them). 
\item The \emph{second stage}: 1913- 1922 (Bohr's quantum theory). 
\item The \emph{third stage}: 1923- 1927 (establishment of quantum mechanics). 
\end{enumerate}
We shall now proceed to study these three stages in detail.  

 The \marginnote{First Experiments and First Attempts to Explain Them (end of 19th century-1912)}foundations of quantum mechanics were laid by
experiments conducted at the end of the 19th century and the beginning
of the 20th century in various branches of physics which at that time
were not connected with one another, e.g. atomic spectroscopy, study
of black body radiation \index{Black body radiation}and the photoelectric effect, solid state physics, study of the structure of atom. By the end of the 19th century a lot of experimental material on the \emph{radiation spectra of atoms} was accumulated. It turned out that atomic spectra are ordered sets of discrete lines (series). In 1885, Balmer \index{Balmer series}discovered a series of lines of atomic hydrogen, later named after him, that could be described by a simple formula. In 1889, Rydberg found a series of lines for thallium and mercury. Extensive studies of the spectra of different atoms were conducted during this period by Kaiser and Runge who used photographic methods. In 1904, Lyman \index{Lyman's series} discovered a series of hydrogen lines falling in the ultraviolet region of the spectrum and in 1909, Paschen found a hydrogen series in the infrared region of the spectrum.\index{Paschen's series} Remarkably, the Lyman and Paschen series could be described by a formula which was very close to the one established earlier by Balmer. Noticing the regularities among various series of an atom, Ritz in 1908 formulated his famous \emph{combination principle}\index{Combination principle, Ritz} (see \sect{sec-02}). However, right until 1913, this formula could not be explained, the nature of the spectral lines remained unclear.

Wien in 1896, studying \emph{black body radiation}, derived a formula which accounted for the experimental results obtained for high radiation
frequencies quite accurately (\emph{Wien's law})\index{Wien's law}. This formula, however, was not applicable for lower frequencies. In 1900, Rayleigh proposed a
formula which agreed fairly well with experiments for low frequencies (the \emph{Rayleigh-Jeans law})\index{Rayleigh-Jeans law} but led to absurd results for higher frequencies (this situation was known as `ultraviolet
catastrophe'). In the same year Lummer and Pringsheim conducted detailed experimental studies over a wide frequency range. In order to explain the results obtained by Lummer and Pringsheim, Planck proposed his famous empirical formula which transformed into Wien's and Rayleigh's formulae in the corresponding limiting cases.  

The formula proposed by Planck contained a certain constant, which he
called an elementary quantum of action (i.e., the Planck's constant
$\hslash$). According to Planck\cite{planck-1955}, 
\begin{quoting}
Either the quantum of action was a fictitious quantity, in which case all the deductions from the
radiation theory were largely illusory and were nothing more than
mathematical juggling. Or the radiation theory is founded on actual
physical ideas, and then the quantum of action must play a fundamental
role in physics, and proclaim itself as something quite new and
hitherto unheard of, forcing us to recast our physical ideas, which,
since the foundation of the infinitesimal calculus by Leibniz and
Newton, were built on the assumption of continuity of all causal
relations.
\end{quoting}

After a careful consideration of his formula, Planck arrived at a
brilliant conclusion: it must be assumed that every radiating atom in
a solid emits energy only \emph{discretely, in quanta}, the energy of an
individual quantum being equal to $\hslash \omega$. This resulted in the historic paper by Planck \emph{Theory of the Law of Distribution of Energy in a Normal Spectrum}, which he presented before the Berlin Academy of
Sciences on 14th December, 1900. This day may, in fact, be considered
as the birthday of quantum mechanics.

Planck's discovery was in
sharp contradiction with the classical theory. It must be admitted
that this was at first disturbing for Planck himself. In an attempt to
reconcile his discovery with classical concepts, Planck proposed in
1911 a hybrid conception. According to this conception, only the
emission process in radiation is discrete while the processes of
propagation and absorption of radiation are continuous ones.

Planck's hybrid hypothesis got no recognition. Earlier, in 1905,
Einstein gave a brilliant explanation of all the laws of the
\emph{photoelectric effect} known at that time by assuming that light is
emitted as well as absorbed discretely. Later, in 1917, Einstein came
to the conclusion that a light quantum possesses not only a definite
\emph{energy} but also a definite \emph{momentum} equal to $\hslash \omega /c$.

In 1907, Einstein successfully applied the idea of quantization to
solve an important problem in solid-state physics which had baffled
scientists for many years. Physicists in the 19th century had faced a
violation of the classical law by Dulong and Petit. It was observed
that the specific heat of solids is not constant but decreases upon
a considerable lowering of temperature. As an example, let us consider
the experiments conducted by Weber in 1875. He discovered a
temperature dependence of the specific heat of boron, carbon and
silicon. The fact that specific heats of solids depend on temperature
could not be explained within the framework of classical theory until
the appearance in 1907 of Einstein's paper \emph{Correlating Planck's
Radiation Theory and the Specific Heat Theory}. Applying Planck's idea
of quantization of energy to atomic vibrations in crystals, Einstein
deduced a formula which, in agreement with experiment, described the
temperature dependence of the specific heat of solids. Einstein's work
Iaid the foundation of the modern theory of the specific heat of
solids.

Finally, we must mention \emph{studies of the atomic structure}
whose origin can be traced to the year 1904 when Thomson proposed a
model for the hydrogen atom in the form of a uniformly positively
charged sphere with one electron at the centre. Later, Thomson came
to the conclusion that the number of electrons in an atom must be
proportional to the atomic weight and that the stability of the atom
is impossible without the motion of the electrons. In 1908 Geiger
and Marsden began studies on the scattering of $\alpha$-particles passing
through thin foils of different metals. They discovered that most of
the $\alpha$-particles pass through the foil without being scattered while
some $\alpha$-particles, roughly one in ten thousand, are strongly
deflected (by an angle greater than \ang{90}). In 1911, Rutherford came
to the conclusion that the strong deflection of an a-particle occurs
not as a result of many collisions, but in a single act of collision
with an atom and, consequently, there must be a small positively
charged nucleus at the centre of the atom, containing almost the
entire mass of the atom. This was a decisive step towards the creation
of the \emph{planetary model of the atom} which Rutherford finally
formulated in 1913.  

Thus, from the end of the 19th century to the year 1913 a large number
of experimental facts, which could not be explained on the basis of
existing theory, were accumulated: the discovery of ordered series in
atomic spectra, the discovery of the quantization of energy in black
body radiation, the photoelectric effect, and the specific heat of
solids; also the planetary model of the atom was created. However,
until 1913, all these discoveries were considered separately. It was
Bohr's genius that understood the common character of these facts and
created a fairly harmonious quantum theory on the atom based on these
facts.

Bohr's famous\marginnote{Bohr's Quantum Theory (1913-1922)}\index{Bohr's! theory}  paper \emph{On the Constitution of Atoms and Molecules} appeared in 1913. It
considered the theory of the planetary model of the hydrogen atom
based on the idea of quantization (the energy and the angular momentum
of an electron in an atom were quantized). Resolutely departing from
accepted concepts, Bohr's theory ruled out a direct link between the
frequency of the radiation emitted by an atom and the frequency of the
rotation of the electron in the atom. Having acquainted himself with
Bohr's theory, Einstein remarked: 
\begin{quoting}
Then the frequency of light
does not depend at all on the frequency of the electron \ldots{} This
is an enormous achievement!
\end{quoting}
 In fact, Bohr's frequency rule provided
a convincing explanation for Ritz' combination principle and permitted
calculations of the Rydberg Constant. Later, in 1945, referring to
Bohr's theory, Einstein wrote: 
\begin{quoting}
This insecure and contradictory
foundation sufficient to enable a man of Bohr's unique instinct and
tact to discover the major laws of spectral lines and of the
electron-shells of the atoms together with their significance for
chemistry appeared to me like a miracle and appears to me as a miracle
even today. This is the highest form of musicality in the sphere of
thought.
\end{quoting}

Experiments conducted in 1914 gave \emph{direct} experimental evidence for
the fact that an atom may change its energy only discretely. These
were the famous experiments by Franck and Hertz on the measurement of
the electron energy spent on exciting mercury atoms.

In 1915-1916, Sommerfeld developed Bohr's theory. In particular, he
generalized the method of quantization for the case of systems having
more than one degree of freedom, by changing from circular orbits to
elliptical ones, and studied the precession of an elliptical orbit in
its own plane. In 1916 Debye and Sommerfeld came to the conclusion
that the angular momentum components in the direction of a magnetic
field are quantized, thus introducing the concept of \emph{quantization in
space}. This received excellent confirmation later in the experiments
of Stern and Gerlach (1921) on the splitting of atomic beams in
nonuniform magnetic fields.

Continuing his researches in the field of quantum theory of atoms,
Bohr in 1918 introduced the famous \emph{correspondence principle}\index{Principle!correspondence} (in his article \emph{On the Quantum Theory of Line Spectra}) which he had been using in fact since 1913. According to this principle, the laws of quantum physics must turn into the laws of classical physics for large values of quantum numbers of a system, i.e. if the relative
values of the quantum of action are negligibly small. It follows from
this that classical physics is of great importance in the discovery of
laws of quantum mechanics, The period from 1913 to the early twenties
can be considered as the period of the creation and development of
Bohr's quantum theory. The achievements of this theory are beyond a
shadow of doubt. It served as the most important step towards the
creation of quantum mechanics. However, in the course of the
development of Bohr's theory, its internal contradictions associated
to a considerable extent with contradictions in the very idea of
quantization and of ``quantum jumps'' (see \sect{sec-02}) became
more and more apparent. The theory clearly betrayed a stage of
crisis. A further development of quantum mechanics demanded an
overcoming of this crisis and the introduction of new ideas. As was
mentioned in \sect{sec-02}, those contradictions were overcome by
introducing the idea of wave-particle duality. By eliminating the
contradictions of the ``old quantum theory'' the idea of duality
marked the beginning of a new stage in the establishment of quantum
mechanics as a genuine theory in physics, culminating in the
creation of its apparatus, and, consequently, in the solution of a
number of problems of atomic and nuclear physics.

 In\marginnote{The Growth of Quantum Mechanics (1923-1927).} 1923,
Compton discovered the effect, later named after him, of a decrease in
the wavelength of X-rays upon scattering by matter. This effect
clearly indicated the existence of wave as well as corpuscular
properties of radiation. Light quanta were introduced into physics as
elementary particles once and for all under the name of
\emph{photons}.

In 1923-1924, de Broglie suggested in his doctoral thesis that the
idea of wave-particle duality should be extended to all
microparticles, associating both wave and corpuscular
characteristics with every particle (see \sect{sec-02}). Later
(in 1927), the idea of duality received a convincing confirmation by
experiments on electron diffraction conducted simultaneously in
several different laboratories. In 1925 de Broglie introduced the
concept of ``matter waves'' described by the so-called \emph{wave function}.\index{Wave function}

The combination of the idea of quantization with the idea of
wave-particle duality proved to be very fruitful for the development
of quantum mechanics, The whole \emph{apparatus} of quantum mechanics
was, in fact, built in 1925-1926. Heisenberg in 1925 took the first
step in this direction. He suggested that every quantized dynamic
variable should be represented in the form of some \emph{matrix} whose
diagonal elements are essentially the experimentally observed values
of the variable (the reader is familiar with this approach for the
case of the Hamiltonian, or energy, matrix which has been discussed
in detail in the book). Using the correspondence principle, Heisenberg converted the matrix relations into classical relations for
corresponding variables. In doing so, however, Heisenberg took into
account the possibility of the commutativity of the product of the
matrices involved. In 1926, Schr\"odinger in his paper \emph{On Quantization as an Eigenvalue Problem} used the wave concepts to introduce his well-known differential equation for a wave function (this equation is now termed the \emph{Schr\"odinger equation}). The problem of calculating the energy levels of a bound microparticle was reduced by Schr\"odinger to the problem of finding eigenvalues.

After the appearance of Schr\"odinger's work, it was at first believed
that we now had two independent theories, Schr\"odinger's \emph{wave
mechanics} and Heisenberg's \emph{matrix mechanics}. However, already
in 1926, Schr\"odinger showed that both these theories are, in fact,
equivalent and are just two different ways of looking at the same
problem. It should be mentioned that the wave formalism of
Schr\"odinger's theory was very well received, since it enabled a
solution of quantum-mechanical problem with the help of established
methods of \emph{mathematical physics}. Planck's opinion\cite{planck-1928} of Schr\"odinger equation is worth noting. According to him, the fundamental importance of this differential equation lies not only in the way it has been
derived, but also in its physical interpretation, whose details are
still not clear. But most important is the fact that owing to the
introduction of the quantum law into the well-known system of usual
differential equations, we get an entirely new method which, with the
help of mathematics, can solve the complicated quantum-mechanical
problem. This is the first case when a quantum of action, which thus
far was impervious to all attempts to look at it from the point of
view of classical physics, can be included in a differential equation.

While the formalism of Schr\"odingers theory was readily accepted, the
problem of the interpretation of wave mechanics and the physical
description of the concept of the ``wave function'' remained the subject
of heated discussion for a long time.\index{Wave function} In 1926, Born proposed a \emph{probability} interpretation of the wave function: ``matter waves''
were replaced by ``probability waves'' The impossibility of interpreting
the wave function as the amplitude of a certain material field (like
the electromagnetic or the gravitational fields) was recognized even
at that time. Planck in 1928, commenting on the nature of the wave
function, wrote\cite{planck-1928} that the impossibility of a physically
intuitive representation of this quantity, which only has an indirect
symbolic meaning, is a direct Consequence of the fact that the wave
motion takes place not in the ordinary three-dimensional space, but in
the so called configurational space, where the dimensionality is
determined by the number of degrees of freedom of the system being
considered.

This meant that de Broglie waves could not be interpreted as classical
waves of some sort.

The next important step towards the development of quantum mechanics, which revealed its physical and philosophical aspects, was made in 1927 by Heisenberg who introduced his famous \emph{uncertainty relation}\index{Uncertainty relations}. Through these relations Heisenberg showed how the concepts of energy, momentum, coordinate, etc. should be applied to the case of microparticles (see \sect{sec-03}). The appearance of the uncertainty relations marked a final break of quantum mechanics from classical determinism, thus establishing quantum mechanics as a statistical theory. Starting from the uncertainty relations, Bohr formulated in the same year one of the leading discoveries of 20th century, i.e. the \emph{complementarity principle} (see \sect{sec-16}).  

The twenties was undoubtedly a period of most intensive development of
quantum mechanics. It is impossible to describe here all the important
researches conducted during this period. We shall simply mention a few
points below to supplement the above picture.

 
\begin{description}[font=\sffamily\bfseries, leftmargin=2cm,
style=sameline]
\item[1924] 
Pauli proposed that an electron could be assigned an
additional (fourth) degree of freedom which can have two
values. Uhlenbeck and Goudsmit, using Pauli's idea, introduced in 1925
the concept of a ``spinning electron'' (in other words, the \emph{spin}
concept).

\item[1924]
Bose carried out fundamental studies, which were
extended by Einstein in the form of a statistical theory for photons
which came to be known as \emph{Bose-Einstein statistics}\index{Bose-Einstein statistics}. In the framework of this theory, Planck's formula for black-body radiation at last found a complete explanation.

\item[1925]
Pauli formulated his famous \emph{exclusion principle} for electrons. 
\index{Pauli's!exclusion principle}\index{Principle! Pauli's exclusion}
\item[1925]
Born and Jordan formulated Heisenberg's theory in matrix
form.  

\item[1925]
Dirac developed the \emph{relativistic} theory of electron and
hydrogen-like atoms.

\item[1926]
Fermi and Dirac carried out fundamental studies on the statistical
theory of electrons, later called the \emph{Fermi-Dirac
statistics}. 
\end{description}

Naturally, the history of the development of quantum
mechanics did not terminate in 1927. In the following years, quantum
mechanics was enriched by many new methods, applications, and most
importantly by further studies of its physical and philosophical
aspects. Some problems of quantum mechanics (above all, the problem of
measurement) are still being investigated. However, we shall end our
brief historical discussion with the year, 1927, assuming that further
developments in quantum mechanics form the subject of a special
study. 

Readers who want to go further into details concerning the
origin and development of quantum mechanics are advised to refer to
these sources.\cite{frankfurt-franc1975, bohr-1962, fierz-1960, podgretsky-1976}
 
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tarasov-qm"
%%% TeX-engine: xetex

%%% End:
