% !TEX root = tarasov-qm.tex

%\part{Appendices}
\begin{appendices}
\begin{center}
\vspace*{10cm}
\textsf{\Huge \drkgry{Appendices}}
\thispagestyle{empty}
\end{center}
   \addappheadtotoc
%   \appendixpage
\label{appendix}
%   
\clearpage
\markboth{Appendices}{}
%\chapter{Appendices} 
%\label{appendix-a}


%\setcounter{section}{0}
%
%\renewcommand*\thesection{Appendix~\Alph{section}:~}
\setcounter{section}{0}% Reset numbering for sections
\renewcommand{\thesection}{\Alph{section}}% Adjust section printing (from here onward)
\section{Eigenvalues and Eigenfunctions of a Hermitian Operator} 
\label{app-A}
\thispagestyle{empty}
%\addcontentsline{toc}{section}{A: Eigenvalues and Eigenfunctions of a Hermitian
%  Operator}

%\newtheorem*{numlessthm2}{Theorem}[section]

\begin{theorem*}
%\marginnote{Eigenvalues and Eigenfunctions of a Hermitian
%  Operator} 
  An operator has real values if and only if it is
Hermitian.
\index{Hermitian operator!in eigen representation}
\nameref{app-A}

\end{theorem*}
\begin{proof}
From \eqref{eq-17.13} we get 
\begin{equation*}
\int \psi^{*} \, (x) \, \hat{L} \, \psi \, (x) \,dx = \lambda \int \psi^{*} \, (x)  \, \psi \, (x) \,dx
\end{equation*}
or
\begin{equation*}
\int \psi \, (x) \, \hat{L^{*}} \, \psi^{*} \, (x) \,dx = \lambda^{*} \int \psi \, (x)  \, \psi^{*} \, (x) \,dx
\end{equation*}
It follows hence that
\begin{equation*}
\int \psi^{*} \, (x) \, \hat{L} \, \psi \, (x)\, dx - \int \psi \, (x)
\, \hat{L^{*}} \, \psi^{*} \, (x) \,dx  = (\lambda - \lambda^{*} )\int \psi^{*} \, (x)  \, \psi \, (x) \,dx
\end{equation*}
Thus 
\begin{equation*}
\int \psi^{*} \, (x) \, \left( \hat{L} - \hat{L^{\dagger}} \right) \, \psi \, (x) \,dx 
  = (\lambda - \lambda^{*} )\int \psi^{*} \, (x)  \, \psi \, (x) \,dx
\end{equation*}
It can be seen from here that the equality $\lambda - \lambda^{*}$
(denoting the real positive eigenvalues) is satisfied if and only if
the operator $\hat{L}$ is Hermitian (i.e. if and only if $\hat{L} - \hat{L^{\dagger}}$). 
\end{proof}

\begin{theorem*}
The eigenfunctions of a Hermitian operator which correspond to
different eigenvalues are mutually orthogonal. 
\end{theorem*}

\begin{proof}
 Using \eqref{eq-17.13}, we write
\begin{equation*}
\hat{L} \, \psi_{n} = \lambda_{n} \, \psi_{n}, \quad \hat{L^{*}} \, \psi^{*}_{m} = \lambda^{*}_{m} \, \psi^{*}_{m}
\end{equation*}
or 
\begin{equation*}
\begin{split}
\int \psi_{m}^{*} \, (x) \, \hat{L} \, \psi_{n} \, (x) dx & =
\lambda_{n} \int \psi_{m}^{*} \, (x)  \, \psi_{n} \, (x) dx\\
\int \psi_{n} \, (x) \, \hat{L^{*}} \, \psi^{*}_{m} \, (x) dx & = \lambda_{m}^{*} \int \psi_{n} \, (x)  \, \psi^{*}_{m} \, (x) dx
\end{split}
\end{equation*}

 It follows from here that
\begin{equation*}
\int \psi^{*}_{m} \, (\hat{L} - \hat{L^{\dagger}})\, \psi_{n} \, dx  = (\lambda_{n} \lambda_{m}^{*})\int \psi^{*}_{m} \, \psi_{n} \, dx
\end{equation*}

If the operator $\hat{L}$ is Hermitian, then $\hat{L} =
\hat{L^{\dagger}}$ (and also $(\lambda_{m}^{*} =  \lambda_{m})$. In this case the last equality acquires the form
\begin{equation*}
(\lambda_{n} - \lambda_{m})\int \psi^{*}_{m} \, \psi_{n} \, dx = 0
\end{equation*}
Since $\lambda_{n} \neq  \lambda_{m}$, we get
\begin{equation*}
\int \psi^{*}_{m} \, \psi_{n} \, dx = 0
\end{equation*}
\end{proof}

\section{Transition from Quantum Mechanics to Classical
  Mechanics}
\label{sec:app-B}
%\addcontentsline{toc}{section}{B: Transition from Quantum Mechanics to Classical
%  Mechanics}


%\marginnote{Transition from Quantum Mechanics to Classical Mechanics}
 There is a formal analogy between the transition from quantum
mechanics to classical mechanics and the transition from wave optics
to geometrical optics (i.e. the optico-mechanical analogy); see, for
example, Landau and Lifshitz\cite{landau-1977}, and Blokhintsev\cite{blokhintsev-1964}. A transition to geometrical
optics means that the optical field is described by nearly plane waves
of the type
\begin{equation}
\exp \, (i \, \Phi)  = \exp  \left( i \, (\vec{k} \, \vec{r} - \omega t) \right)
\tag{B.1}
\label{B.1}
\end{equation}
It can be seen from \eqref{B.1} that
\begin{equation}
\vec{k} = \nabla \, \Phi \quad \omega = - \frac{\partial
  \Phi}{\partial t}
\tag{B.2}
\label{B.2}
\end{equation}
The vector $\vec{k}$ is directed along the light beam. At every point
in space the light beam is perpendicular to the surface of constant
phase (i.e. perpendicular to the surface $\Phi  =$ const).

When going over to classical mechanics, the quantum-mechanical wave function must acquire a form analogous to \eqref{B.1}; moreover, the phase of the wave function will be proportional to the mechanical action $S$:
\begin{equation*}
\Phi = AS.
\end{equation*}
The momentum $\vec{p}$ and energy $E$ of a particle are expressed through
the action $S$ in the following way:
\begin{equation}
\vec{p} = \nabla S, \quad E  = - \frac{\partial S}{\partial t}
\tag{B.3}
\label{B.3}
\end{equation}
The trajectories are lines perpendicular to the surface of constant
action (in the same way as the light rays are perpendicular to the
surface of constant phase). Comparing \eqref{B.2} and \eqref{B.3} and taking
into account that $\vec{p} = \hbar \vec{k}$, we find that
\begin{equation*}
\Phi  =  \frac{ S}{\hbar}
%\tag{B.4}
\end{equation*}
Thus the quasi-classical wave function is of the form
\begin{equation}
\psi  = a \, \exp \left( \frac{i \, S}{\hbar}\right)
\tag{B.4}
\label{B.4}
\end{equation}
By using \eqref{B.4} it is easy to determine the coefficient $\gamma$ in the expression for the momentum operator
\begin{equation*}
\vec{p} = \gamma \nabla
\end{equation*}
Let us consider the equation for the eigenfunctions of the momentum
operator (the equation $\hat{\vec{p}} \psi = \vec{p \psi}$) in the
quasiclassical case. In this case we get $\vec{p}= \nabla S, \,\, \psi =  a \, \exp
\left( \frac{i \, S}{\hbar}\right) $ Thus the above equation assumes  the form 
\begin{equation*}
\gamma  \nabla \exp \left( \frac{i \, S}{\hbar}\right) =  \nabla S \,  \exp \left( \frac{i \, S}{\hbar}\right)
\end{equation*}
or
\begin{equation*}
\gamma  \exp \left( \frac{i \, S}{\hbar}\right) \, \nabla S
\frac{i}{\hbar} = \nabla S \,  \cdot \exp \left( \frac{i \, S}{\hbar}\right)
\end{equation*}
It can be easily seen that $i\, \gamma / \hbar = 1$, and, consequently, $\gamma = -
i \hbar$. The quasi-classical case may he used to substantiate the
Schr\"odinger equation. It can be easily seen that the well-known
classical Hamilton-Jacobi equation
\begin{equation*}
\frac{\partial S}{\partial t} + \frac{1}{2m} (\nabla S)^{2} + U = 0
\end{equation*}
is a limiting case of the Schr\"odinger equation \eqref{eq-20.17}. By
putting $\Psi = \exp \left( \frac{i \, S}{\hbar}\right)$, we get in this case
\begin{equation*}
\begin{split}
i \, \hbar \frac{\partial }{\partial t} \Psi & = - \frac{\partial
  S}{\partial t} \exp \left( \frac{i \, S}{\hbar}\right) \\
- \frac{\hbar^{2}}{2m} \, \Delta \Psi & = - \frac{\hbar^{2}}{2m} \nabla(\nabla \Psi)\\
& =   - \frac{\hbar^{2}}{2m} \, \nabla \left( \frac{i}{\hbar} \,
  \nabla \, S \, \cdot \exp \left( \frac{i \, S}{\hbar}\right) \right)\\
& = - \frac{\hbar}{2m} \nabla S \, \cdot \exp \left( \frac{i \,
    S}{\hbar}\right) + \frac{1}{2m} (\nabla S)^{2} \exp \left( \frac{i \, S}{\hbar}\right)
\end{split}
\end{equation*}
Substituting these results into \eqref{eq-20.17} and neglecting terms
containing Planck's constant in this expression for $\Delta \Psi$, we get the
equation
\begin{equation*}
- \frac{\partial S}{\partial t} \exp \left( \frac{i \, S}{\hbar}\right) = \frac{1}{2m} (\nabla S)^{2} \exp \left( \frac{i \, S}{\hbar}\right) + U \, \exp \left( \frac{i \, S}{\hbar}\right)
\end{equation*}
By cancelling out the factor $\exp (iS/ \hbar)$, this equation becomes the Hamilton-Jacobi equation.


\section{Commutation Relations} 
%\addcontentsline{toc}{section}{C: Commutation Relations}
\label{app-C}
% \marginnote{Commutation Relation}
We\index{Commutation relations} shall show how the commutation
relations \eqref{eq-20.28}-\eqref{eq-20.30} can be derived. To start
with we remark that the equality $\hat{\vec{M}} = (\hat{\vec{r}}
\times \hat{\vec{p}})$ may be written in the form 
\begin{equation}
\hat{M_{i}} = \sum_{k=1}^{3} \, \sum_{n=1}^{3} e_{ikn} \, \hat{r_{k} }
\, \hat{p_{n}}
\tag{C.1}
\label{C.1}
\end{equation}
where $e_{ikn}$ is a unit antisymmetric tensor of the 3rd rank
($e_{123}= e_{231} = e_{312} =1$, $e_{132}= e_{321} = e_{213} =- 1$, all the ether components of the tensor are equal to zero).

Let us consider the commutator $\left[ \hat{M_{i}}, \, \hat{r_{j}} \right]$. By using \eqref{C.1}, we can write this commutator in the following form:
\begin{equation*}
\begin{split}
\left[ \hat{M_{i}}, \, \hat{r_{j}} \right] & = \hat{M_{i}} \,
\hat{r_{j}} - \hat{r_{j}} \, \hat{M_{i}}	 \\
& = \sum_{k} \, \sum_{n} \, e_{ikn} \left(
  \hat{r_{k}} \, \hat{p_{n}} \, \hat{r_{j}} -\hat{r_{j}} \,
  \hat{r_{k}} \, \hat{p_{n}}
\right)
\end{split}
\end{equation*}
Taking into account that $\hat{r_{j}}\, \hat{r_{k}} = \hat{r_{k}} \,
\hat{r_{j}}$, we get 
\begin{equation*}%
\left[ \hat{M_{i}}, \, \hat{r_{j}} \right]  = \sum_{k} \, \sum_{n} \,
e_{ikn} \, \hat{r_{k}} \,  \left[
   \hat{p_{n}}, \, \hat{r_{j}} \right]
\end{equation*}
According to \eqref{eq-20.26},
$\left[ \hat{p_{n}}, \, \hat{r_{j}} \right] = -i \hbar
\delta_{nj}$. This leads to the required relation \eqref{eq-20.28}:
\begin{equation*}%
\begin{split}
\left[ \hat{M_{i}}, \, \hat{r_{j}} \right] &  =i \hbar \,  \sum_{k} \,
e_{ikj} \, \hat{r_{k}} \\
& = i \hbar \sum_{k} \, e_{ijk} \, \hat{r_{k}} 
\end{split}
\end{equation*}
This relation means that
\begin{equation*}%
\begin{split}
\left[ \hat{M_{1}}, \, \hat{r_{2}} \right]  & = i \hbar \,  \hat{r_{3}} \\
\left[ \hat{M_{2}}, \, \hat{r_{3}} \right] &  = i \hbar \,  \hat{r_{1}} \\
\left[ \hat{M_{3}}, \, \hat{r_{1}} \right] &  = i \hbar \,  \hat{r_{2}} \\
\left[ \hat{M_{1}}, \, \hat{r_{1}} \right] &  = \left[ \hat{M_{2}}, \,  \hat{r_{2}} \right]  
= \left[ \hat{M_{3}}, \, \hat{r_{3}} \right] = 0
\end{split}
\end{equation*}
Next let us consider the commutator $\left[ \hat{M_{i}}, \, \hat{p_{j}} \right] $. By using \eqref{C.1}, we can write it in the form
\begin{equation*}
\left[ \hat{M_{i}}, \, \hat{p_{j}} \right]  = \sum_{k} \, \sum_{n} \,
e_{ikn} \,  \left(
  \hat{r_{k}} \, \hat{p_{n}} \, \hat{p_{j}} -\hat{p_{j}} \,
  \hat{r_{k}} \, \hat{p_{n}} \right)
\end{equation*}
Taking into account that $\hat{p_{n}} \, \hat{p_{j}} =
\hat{p_{j}} \, \hat{p_{n}}$, we get 
\begin{equation*}
\left[ \hat{M_{i}}, \, \hat{p_{j}} \right]  = \sum_{k} \, \sum_{n} \,
e_{ikn} \,  \left[  \hat{r_{k}}, \, \hat{p_{j}} \right]  \hat{p_{n}}
\end{equation*}
According to \eqref{eq-20.26}, $ \left[  \hat{r_{k}}, \, \hat{p_{j}}
\right] = i \hbar \delta_{kj}$. As a result, we get the required relation \eqref{eq-20.29}:
\begin{equation*}
\left[ \hat{M_{i}}, \, \hat{p_{j}} \right]  = i \, \hbar \, \sum_{n} \,
e_{ijn} \,  \hat{p_{n}}
\end{equation*}
Thus
\begin{equation*}
\begin{split}
\left[ \hat{M_{1}}, \, \hat{p_{2}} \right] &  = i \, \hbar \, \hat{p_{3}} \\
\left[ \hat{M_{2}}, \, \hat{p_{3}} \right] &  = i \, \hbar \, \hat{p_{1}} \\
\left[ \hat{M_{3}}, \, \hat{p_{1}} \right] &  = i \, \hbar \, \hat{p_{2}} \\
\left[ \hat{M_{1}}, \, \hat{p_{1}} \right] &  = \left[ \hat{M_{2}}, \,  \hat{p_{2}} \right]  
= \left[ \hat{M_{3}}, \, \hat{p_{3}} \right] = 0
\end{split}
\end{equation*}
Going over to the commutator $\left[ \hat{M_{i}}, \, \hat{M_{j}} \right] $, we shall restrict ourselves for simplicity to the case when$ i = 1, j = 2$. Using 
\begin{equation*}
\begin{split}
 \hat{M_{1}} & = \hat{r_{2}} \, \hat{p_{3}} - \hat{r_{3}}  \, \hat{p_{2}}\\
 \hat{M_{2}} & = \hat{r_{3}} \, \hat{p_{1}} - \hat{r_{1}}  \, \hat{p_{3}}
\end{split}
\end{equation*}
we can write
\begin{equation*}
\begin{split}
\left[ \hat{M_{1}}, \, \hat{M_{2}} \right] & = (\hat{r_{2}} \,
\hat{p_{3}} - \hat{r_{3}}  \, \hat{p_{2}}) \, (\hat{r_{3}} \,
\hat{p_{1}} - \hat{r_{1}}  \, \hat{p_{3}}) - (\hat{r_{3}} \,
\hat{p_{1}} - \hat{r_{1}}  \, \hat{p_{3}}) \times (\hat{r_{2}} \, \hat{p_{3}}
- \hat{r_{3}}  \, \hat{p_{2}}) \\
& =  \hat{r_{2}}  \, \hat{p_{3}} \, \hat{r_{3}}  \, \hat{p_{1}} -
\hat{r_{2}}  \, \hat{p_{3}} \, \hat{r_{1}}  \, \hat{p_{3}} - \hat{r_{3}}
\, \hat{p_{2}}, \hat{r_{3}}  \, \hat{p_{1}} + \hat{r_{3}}  \,
\hat{p_{2}} \, \hat{r_{1}}  \, \hat{p_{3}} - \hat{r_{3}}  \,
\hat{p_{1}}\, \hat{r_{2}}  \, \hat{p_{3}} \\
& + \hat{r_{3}}  \, \hat{p_{1}} \, \hat{r_{3}}  \, \hat{p_{2}} + \hat{r_{1}}  \,
\hat{p_{3}} \, \hat{r_{2}}  \, \hat{p_{3}} - \hat{r_{1}}  \,
\hat{p_{3}} \, \hat{r_{3}}  \, \hat{p_{2}}
\end{split}
\end{equation*}
Note that $ \hat{r_{2}}  \, \hat{p_{3}} \, \hat{r_{1}}  \,
\hat{p_{3}} = \hat{r_{1}}  \, \hat{p_{3}} \, \hat{r_{2}}  \,
\hat{p_{3}}  $ and  $ \hat{r_{3}}  \, \hat{p_{2}} \, \hat{r_{3}}  \,
\hat{p_{1}} = \hat{r_{3}}  \, \hat{p_{1}} \, \hat{r_{3}}  \,
\hat{p_{2}}  $.
By taking this into account, we write the expression for $ \left[ \hat{M_{1}}, \, \hat{M_{2}} \right]$: 
\begin{equation*}
\begin{split}
\left[ \hat{M_{1}}, \, \hat{M_{2}} \right] & = (\hat{r_{2}} \,
\hat{p_{3}}  \, \hat{r_{3}}  \, \hat{p_{1}} -  \hat{r_{3}} \,
\hat{p_{1}} \, \hat{r_{2}}  \, \hat{p_{3}}) +  (\hat{r_{3}} \,
\hat{p_{2}}  \, \hat{r_{1}}  \, \hat{p_{3}} -  \hat{r_{1}} \,
\hat{p_{3}} \, \hat{r_{3}}  \, \hat{p_{2}}) \\
& =  \hat{r_{2}}  \, \hat{p_{1}} \left(  \hat{p_{3}}  \, \hat{r_{3}} -
 \hat{r_{3}}  \, \hat{p_{3}} \right) + \hat{r_{1}}  \, \hat{p_{2}}
\left(  \hat{r_{3}}  \, \hat{p_{3}} -  \hat{p_{3}}  \, \hat{r_{3}} \right)
\end{split}
\end{equation*}
Since $ \hat{r_{3}}  \, \hat{p_{3}} -  \hat{p_{3}}  \, \hat{r_{3}} = i \hbar $, we finally obtain
\begin{equation*}
\begin{split}
\left[ \hat{M_{1}}, \, \hat{M_{2}} \right] & =  i \hbar \, \left(
\hat{r_{1}}  \, \hat{p_{2}} -  \hat{r_{2}}  \, \hat{p_{1}} \right)\\
& =  i \hbar \, \hat{M_{3}} 
\end{split}
\end{equation*}
Proceeding in the same way, it can be easily seen that
\begin{equation*}
\begin{split}
\left[ \hat{M_{2}}, \, \hat{M_{3}} \right] & =  i \hbar \, \hat{M_{1}}\\
\left[ \hat{M_{3}}, \, \hat{M_{1}} \right] & =  i \hbar \, \hat{M_{2}} 
\end{split}
\end{equation*}
All these results can be combined in the relation \eqref{eq-20.30}.

\section{Commutation of Operators $\hat{M}^{2}$,  $\hat{M_{i}}$} 
%\addcontentsline{toc}{section}{D: Commutation of Operators $\hat{M}^{2}$,  $\hat{M_{i}}$}
\index{Commutation of operators}
%\marginnote{ Commutation of Operators $\hat{M}^{2}$,  $\hat{M_{i}}$}
\label{app-D}
Let us consider the case when $i = 1$: 
\begin{equation*}
\begin{split}
[\hat{M}^{2}, \hat{M}_{1}] & = (\hat{M}_{1}^{2} + \hat{M}_{2}^{2} +
\hat{M}_{3}^{2}) \, \hat{M}_{1}- (\hat{M}_{1} \, \hat{M}_{1}^{2} +
\hat{M}_{2}^{2} + \hat{M}_{3}^{2}) \\
& = (\hat{M}_{2}^{2} + \hat{M}_{3}^{2} ) \, \hat{M}_{1} - \,
\hat{M}_{1}(\hat{M}_{2}^{2} + \hat{M}_{3}^{2}) \\
& = \hat{M}_{2} \hat{M}_{2} \hat{M}_{1} +  \hat{M}_{3} \hat{M}_{3}
\hat{M}_{1}  -  \hat{M}_{1} \hat{M}_{2} \hat{M}_{2}  -  \hat{M}_{1}
\hat{M}_{3} \hat{M}_{3} \\
& = ( \hat{M}_{2} \hat{M}_{1} \hat{M}_{2}  - i \, \hbar \,  \hat{M}_{2}
\hat{M}_{3}) + ( \hat{M}_{3} \hat{M}_{1} \hat{M}_{3}  + i \, \hbar \,  \hat{M}_{3}
\hat{M}_{2}  ) \\
& - ( \hat{M}_{2} \hat{M}_{1} \hat{M}_{2}  +
i \hbar \, \hat{M}_{3} \hat{M}_{2} ) - ( \hat{M}_{3} \hat{M}_{1}
\hat{M}_{3}  - i \, \hbar \,  \hat{M}_{2} \hat{M}_{3} ) \\
 & = 0
\end{split}
\end{equation*}
Similarly, we get $[\hat{M}^{2}, \hat{M}_{2}] = 0$ and  $[\hat{M}^{2}, \hat{M}_{3}] = 0$. 
This proves the relation \eqref{eq-20.31}.
\section{Some Special Functions}
%\addcontentsline{toc}{section}{E: Some Special Functions}
%\marginnote{Some Special Functions}
\label{app-E}\index{Special functions}
\emph{Legendre Polynomials}:\index{Legendre's!polynomials} The Legendre polynomials $P_{l} \,(x)$ may be defined as the coefficients in the expansion of the function
\begin{equation*}%
H\, (x, \, t) = \frac{1}{\sqrt{1 + t^{2} - 2tx}}, \quad |\, t \, | < 1
\end{equation*}
as a power series in $t$:
\begin{equation}%
 \frac{1}{\sqrt{1 + t^{2} - 2tx}} = \sum_{l =0}^{\infty} \, P_{l} \,
 (x) \, t^{n}
\tag{E.1}
\label{E.1}
\end{equation}
($- 1 \leq x \leq 1$, $l$ is a non-negative integer).

The polynomials $P_{l}\, (x)$ are the solutions of the differential
equation 
\begin{equation}%
(1-x^{2}) y''  - 2xy' + l (l+ 1) y= 0, \quad  (l=0, 1,2, \ldots)
\tag{E.2}
\label{E.2}
\end{equation}
and satisfy the condition $y\, (1) = 1$. They can he written in the form
\begin{equation}%
P_{l}\, (x) = \frac{1}{2^{l}\, l!} \, \frac{d^{l}}{dx^{l}} \left[
  (x^{2} -1)^{l} \right]
\tag{E.3}
\label{E.3}
\end{equation}
We note that
\begin{equation*}
\begin{split}
P_{l}\,(1) & = 1, \quad P_{l}\, (-x) = (-1)^{l} P_{l}\,(x) \\
P_{0}(x) & =  1 \\
P_{1}(x) & = x \\
P_{2}(x) & = \frac{1}{2}( 3x^{2} - 1) \\
P_{3}(x) & = \frac{1}{2} (5 x^{3} - 3x) \\
\ldots & \ldots
\end{split}
\end{equation*}
The Legendre polynomials are orthonormalized:
\begin{equation}%
\int\limits_{-1}^{1}  P_{l} \, (x) \, P_{l'} \, (x) \,
dx = \frac{2}{2l +1}  \delta_{ll'}
\tag{E.4}
\label{E.4}
\end{equation}
The basic recurrence relations are 
\begin{equation*}
\begin{split}
(l+ 1) \, P_{l+1} + l \, P_{l-1} -x \, (2l +1) \, P_{l} & = 0 \\
P_{l} - P_{l+1}' - P_{l-1}' + 2xP_{l}' & = 0 \\
P_{l+1}' - P_{l-1}' & = (2l+1) P_{l}
\end{split}
\end{equation*}



\emph{Associated Legendre Functions}: The associated Legendre
functions $p_{m}^{l} \, (x)$ are defined through the Legendre polynomials $P_{l} (x)$
in the following way:
\begin{equation}%
p_{l}^{m} \, (x) = (1 - x^{2})^{\frac{m}{2}} \, \frac{d^{m}}{dx^{m}}
P_{l}\, (x)
\tag{E.5}
\label{E.5}
\end{equation}
($m$ and $l$ are positive integers, $m \leq  1$),
\begin{equation*}
\begin{split}
p_{l}^{0}(x) & = P_{l}(x);	\quad p_{l}^{l} = 3 \times 5 \times
\times 7 \ldots (2l -1) \,(1-x^{2})^{l/2}  \\
p_{1}^{1}(x) & =  (1 - x^{2})^{1/2} \\
p_{2}^{1}(x) & = 3x \,  (1 - x^{2})^{1/2}\\
p_{3}^{2}(x) & = 3 (1 - x^{2})
\ldots 
\end{split}
\end{equation*}
The functions  $p_{m}^{l} \, (x)$ satisfy the differential equation
\begin{equation}%
(1 - x^{2}) y'' - 2xy' + \left[ l(l+1) - \frac{m^{2}}{1- x^{2}}
\right]  y = 0
\tag{E.6}
\label{E.6}
\end{equation}
They are orthonormalized:
\begin{equation}%
\int\limits_{-1}^{1}  p_{l}^{m} \, (x) \, (x) \, p_{l'}^{m} \, (x) \,
dx = \frac{2}{2l +1} \frac{(l+m)!}{(l-m)!} \delta_{ll'}  = 0
\tag{E.7}
\label{E.7}
\end{equation}
The basic recurrence relations are 
\begin{equation*}
\begin{split}
(2l+ 1) \, x \, p_{l}^{m}\, (x) & = (l- m+ 1)  p_{l+1}^{m} \, (x) +
                                   (l+ m) p_{l-1}^{m} (x) \\
p_{l+1}^{m+1}\, (x) - p_{l-1}^{m+1}\, (x)  &=(2l +1 ) \, \sqrt{1 - x^{2}}
                                             p_{l}^{m} \, (x) 
\end{split}
\end{equation*}

\emph{Harmonic Functions}\index{Harmonic functions}. The harmonic function $Y_{lm}(\theta, \,
\varphi)$ is defined as the product of the Legendre associated
function $p_{l}^{|m|}( \cos \theta)$  and the function $\exp \, (i m \varphi)$:
\begin{equation}%
Y_{lm} \, (\theta, \, \varphi) =  \sqrt{\frac{2l + 1(l - |m|)!}{4 \pi (l + |m|)!}} p_{l}^{|m|} \, ( \cos
\theta) \, \exp \, (i m \varphi)
\tag{E.8}
\label{E.8}
\end{equation}

($l$ is a non-negative integer, $m = 0, \pm 1,\pm 2, \ldots  \pm l$,
$\theta$ and $\varphi$ are angular coordinates, $0 \leq \theta \leq
\pi$, $0 \leq \varphi \leq 2 \pi $). Expressions for the first few
harmonic functions are given in Section \ref{sec-21} (see
\eqref{eq-21.26a} - \eqref{eq-21.26c}).

 The harmonic functions satisfy the differential equation
\begin{equation}%
\Delta_{\theta \, \varphi} \, y + l(l+1) y = 0
\tag{E.9}
\label{E.9}
\end{equation}
where
\begin{equation*}
\Delta_{\theta \, \varphi} = \frac{1}{\sin^{2}\theta}
\frac{\partial}{\partial \varphi^{2}} + \frac{1}{\sin \theta}
\frac{\partial}{\partial \theta} \left( \sin \theta \,
  \frac{\partial}{\partial \theta} \right)
\end{equation*}
The spherical functions are orthonormalized: 
\begin{equation}%
\int\limits_{0}^{2 \pi} \int\limits_{0}^{\pi}   Y_{lm}^{*}(\theta, \,
\varphi) \,  Y_{l' m'}
(\theta, \,  \varphi) \, \sin \theta \, d \theta \, d \, \varphi = \delta_{ll'}  \, \delta_{mm'} 
\tag{E.10}
\label{E.10}
\end{equation}



\emph{Hermite Polynomials}\index{Hermite polynomials}. The Hermite polynomials $H_{n}(x)$ may be
defined as expansion coefficients of the function $ \exp \, (2xt - t^{2})$:
\begin{equation}%
\exp \, (2xt - t^{2}) = \sum_{n=0}^{\infty}  H_{n} \, (x) \frac{t^{n}}{n!}
\tag{E.11}
\label{E.11}
\end{equation}
($ - \infty < x < \infty $, $n$ is a non-negative integer). 


Hermite polynomials are described by the expression:
\begin{equation}%
H_{n} \, (- x) = (- 1)^{n} \exp \, (x^{2}) \,  \frac{d^{n}}{dx^{n}} \exp \, (-x^{2})
\tag{E.12}
\label{E.12}
\end{equation}
Some of the values are 
\begin{equation*}
\begin{split}
H_{n}\, (- x) & =  (- 1)^{n} \, H_{n} \, (x) \\
H_{0} \, ( x) & = 1\\
 H_{1}\, ( x) & = 2x \\
 H_{2}\, (x) & = 4 x^{2} -2  \\
 H_{3}\, (x) & = 8 x^{3} - 12 x \\
 \ldots & \ldots
\end{split}
\end{equation*}
The polynomials $H_{n} \,(x)$ satisfy the differential equation
\begin{equation}%
y'' - 2xy' + 2ny = 0 \quad  (n=0, 1, 2, \ldots )
\tag{E.13}
\label{E.13}
\end{equation}
They are orthonormalized:
\begin{equation}%
\int\limits_{- \infty}^{\infty} H_{n} \, (x) \,  H_{n'} \, (x) \,
\exp \, \left({-x^{2}} \right) \, dx =  \sqrt{\pi} \, 2^{n} \, n! \, \delta_{nn'} 
\tag{E.14}
\label{E.14}
\end{equation}
The basic recurrence relations are
\begin{equation*}
\begin{split}
2\, x \, H_{n} - 2n \, H_{n-1} & = H_{n+1} \\
2n \, H_{n-1} & = H_{n}'
\end{split}
\end{equation*}

\emph{Modified Hermite Polynomials. (Hermite-Gauss Polynomials)}. The
Hermite-Gauss polynomials $h_{n} (x)$ are defined in terms of the
Hermite polynomials $H_{n} (x)$ by the formula 
\begin{equation}%
h_{n} \, (x) = \frac{1}{\sqrt{2^{n}\, n! \sqrt{\pi}}} H_{n} \, (x)
\exp \, \left( - \frac{x^{2}}{2} \right) 
\tag{E.15}
\label{E.15}
\end{equation}
 The condition of orthonormalization for them has the form
\begin{equation}%
\int\limits_{- \infty}^{\infty} h_{n} \, (x) \,  h_{n'} \, (x) \, dx =
\delta_{nn'}
\tag{E.16}
\label{E.16}
\end{equation}
These polynomials satisfy the differential equation
\begin{equation}%
y'' + (2n + 1 - x^{2}) y = 0 
\tag{E.17}
\label{E.17}
\end{equation}

\section{Linear Harmonic Oscillator} 
%\addcontentsline{toc}{section}{F: Linear Harmonic Oscillator}
%\marginnote{Linear Harmonic Oscillator} 

\label{app-F}\index{Linear harmonic oscillator(s)}
By taking \eqref{eq-22.1} into account, we can write Schr\"odinger's equation
\begin{equation}%
\varphi'' (x) + \frac{2m}{\hbar^{2}} \, E \, \varphi \, (x) - \frac{m^{2}
\omega^{2}}{\hbar^{2}} x^{2} \, \varphi \, (x) = 0
\tag{F.1}
\label{F.1}
%\label{eq-F.1}
\end{equation}
We introduce the notation
\begin{equation*}%
\xi = x \sqrt{\frac{m \omega}{\hbar}}, \quad \lambda = \frac{2E}{\hbar
\omega}
\end{equation*}

In the new notation, \eqref{F.1} acquires the form
\begin{equation}%
\varphi'' (\xi) + (\lambda - \xi^{2}) \, \varphi \, (\xi) = 0
\tag{F.2}
\label{F.2}
%\label{eq-F.1}
\end{equation}
The differential equation for Hermite-Gauss polynomials is of the same
form (see \eqref{E.17}). Moreover, $\lambda = 2n + 1$ and, consequently
\begin{equation*}%
 E= \hbar \omega \left( n + \frac{1}{2} \right), \quad n=0, 1, 2,
\end{equation*}
The wave function $\varphi_{n} \, (\xi)$ is apart from a constant factor a Hermite-Gauss polynomial:
\begin{equation*}%
\varphi_{n}\, (\xi) = C \, h_{n} \, (\xi)
\end{equation*}
The factor $C$ is determined from the normalization condition for the
wave function $\varphi_{n} \, (x)$:
\begin{equation*}%
\int\limits_{- \infty}^{\infty} \varphi_{n}^{2} \, (x) \,dx=1
\end{equation*}
Thus 
\begin{equation*}%
\begin{split}
1 & = \int\limits_{- \infty}^{\infty} \varphi_{n}^{2} \, (x) \, dx \\
   & = \int\limits_{- \infty}^{\infty} \varphi_{n}^{2} \, (\xi) \,
   \frac{dx}{d\xi}  d\xi \\
& = \sqrt{\frac{\hbar}{m \omega}}  \int\limits_{- \infty}^{\infty}
\varphi_{n}^{2} \, (\xi) \,  d\xi \\
& = \sqrt{\frac{\hbar}{m \omega}}  C^{2} \int\limits_{- \infty}^{\infty}
h_{n}^{2} \, (\xi) \,  d\xi 
\end{split}
\end{equation*}
Taking \eqref{E.16} into account we conclude that 
\begin{equation*}
\sqrt{\frac{\hbar}{m \omega}}C^{2} = 1, \quad \textrm{and, \,\,\,
  consequently,} \quad C  = \sqrt[4]{\frac{m \omega}{\hbar}}
\end{equation*}
Thus we arrive at the relation \eqref{eq-22.3}
\begin{equation*}
\begin{split}
\varphi_{n} & = \sqrt[4]{\frac{m \omega}{\hbar}} h_{n} \, (\xi) \\
& = \sqrt[4]{\frac{m \omega}{\hbar}} \exp \, \left(
  -\frac{\xi^{2}}{2}\right)  \, H_{n}\, (\xi)
\frac{1}{\sqrt{2^{n} \cdot n! \sqrt{\pi}}}
\end{split}
\end{equation*}
 \end{appendices}

%\markboth{Index}{}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tarasov-qm"
%%% TeX-engine: xetex
%%% End:
