(TeX-add-style-hook
 "01-physics-of-microparticles"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (LaTeX-add-labels
    "ch-01"
    "sec-01"
    "eq-1.1"
    "eq-1.2"
    "eq-1.3"
    "sec-02"
    "fig-2.1"
    "eq-2.1"
    "eq-2.2"
    "eq-2.3"
    "eq-2.4"
    "eq-2.5a"
    "eq-2.5b"
    "eq-2.6"
    "eq-2.7"
    "eq-2.8"
    "eq-2.9a"
    "eq-2.9b"
    "eq-2.10"
    "eq-2.11"
    "eq-2.12"
    "eq-2.13"
    "sec-03"
    "eq-3.1"
    "fig-3.1"
    "eq-3.2"
    "eq-3.3"
    "eq-3.4"
    "eq-3.3a"
    "eq-3.4a"
    "eq-3.2a"
    "eq-3.5"
    "fig-3.2"
    "eq-3.5a"
    "eq-3.6a"
    "eq-3.6b"
    "eq-3.6c"
    "eq-3.7a"
    "eq-3.7b"
    "eq-3.8"
    "sec-04"
    "eq-4.1"
    "eq-4.2"
    "eq-4.3"
    "eq-4.4"
    "eq-4.5"
    "eq-4.6"
    "eq-4.7"
    "eq-4.8"
    "eq-4.9"
    "eq-4.10"
    "eq-4.11"
    "fig-4.1"
    "fig-4.2"
    "sec-05"
    "eq-5.1"
    "eq-5.2"
    "fig-5.1"
    "eq-5.3a"
    "fig-5.2"
    "eq-5.4"
    "sec-06"
    "fig-6.1"
    "fig-6.2"
    "interlude-02"))
 :latex)

