(TeX-add-style-hook
 "05-appendices"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (LaTeX-add-labels
    "appendix-a"
    "app-A"
    "app-B"
    "B.1"
    "B.2"
    "B.3"
    "B.4"
    "app-C"
    "C.1"
    "app-D"
    "app-E"
    "E.1"
    "E.2"
    "E.3"
    "E.4"
    "E.5"
    "E.6"
    "E.7"
    "E.8"
    "E.9"
    "E.10"
    "E.11"
    "E.12"
    "E.13"
    "E.14"
    "E.15"
    "E.16"
    "E.17"
    "app-F"
    "F.1"
    "F.2")
   (LaTeX-add-amsthm-newtheorems
    "numlessthm")))

