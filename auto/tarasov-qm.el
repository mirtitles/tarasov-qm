(TeX-add-style-hook
 "tarasov-qm"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("tufte-book" "a4paper" "sfsidenotes" "colorlinks=true")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("newtxmath" "libertine") ("enumitem" "inline") ("tcolorbox" "most")))
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (TeX-run-style-hooks
    "latex2e"
    "00-preface"
    "00-prelude"
    "01-physics-of-microparticles"
    "02-physical-foundations-of-qm"
    "03-linear-operators-in-qm"
    "04-brief-historical-survey"
    "05-appendices"
    "06-index-ref"
    "tufte-book"
    "tufte-book10"
    "fontspec"
    "newtxmath"
    "libertine"
    "fancyvrb"
    "nicefrac"
    "mdframed"
    "booktabs"
    "microtype"
    "aurical"
    "soul"
    "siunitx"
    "amsmath"
    "amsfonts"
    "mathrsfs"
    "mathtools"
    "graphicx"
    "float"
    "textcomp"
    "enumitem"
    "titlesec"
    "pdfpages"
    "chngcntr"
    "xltxtra"
    "calrsfs"
    "braket"
    "color"
    "amsthm"
    "tcolorbox"
    "setspace"
    "emptypage"
    "relsize"
    "makeidx"
    "ellipsis")
   (TeX-add-symbols
    "Ea"
    "openbox")
   (LaTeX-add-environments
    "quoteOld")
   (LaTeX-add-bibliographies))
 :latex)

