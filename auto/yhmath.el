(TeX-add-style-hook
 "yhmath"
 (lambda ()
   (TeX-run-style-hooks
    "amsmath")
   (TeX-add-symbols
    '("widering" 1)
    "widetriangle"
    "wideparen"
    "adots")
   (LaTeX-add-environments
    "amatrix")))

